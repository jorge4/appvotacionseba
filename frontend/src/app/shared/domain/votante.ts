import {Puntero} from './puntero';

export class Votante {
    constructor(public id: number,
                public mesa: number,
                public orden: number,
                public carnet: string,
                public nombre: string,
                public ci: string,
                public telefono: string,
                public puntero: Puntero,
                public voto_registrado: boolean,
    ) {
    }
}
