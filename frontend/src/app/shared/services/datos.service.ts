import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import {ServiceResponse} from '../domain/service-response';
import {Observable, Subject} from 'rxjs';
import {MessageService} from 'primeng/api';

@Injectable()
export class DatosService extends BaseService {

    private baseUrl = '/api';
    public readonly uploadUrl = this.baseAPIUrl + '/api/padron/subir';

    constructor(private http: HttpClient, public m_s: MessageService) {
        super(http, m_s);
    }


    votar(body: any): Observable<ServiceResponse> {
        return this.post(this.baseUrl + '/votar', body);
    }

    faltantes(body: any): Observable<ServiceResponse> {
        return this.get(this.baseUrl + '/faltantes', body);
    }

    padronstats(body: any): Observable<ServiceResponse> {
        return this.get(this.baseUrl + '/padron/stats', body);
    }

    superstats(body: any): Observable<ServiceResponse> {
        return this.get(this.baseUrl + '/padron/superstats', body);
    }

    procesarUpload(xhr: XMLHttpRequest): number[] {
        const sub = new Subject<ServiceResponse>();
        const rta = <ServiceResponse>(<any>xhr).body;
        this.doMessaging(sub.asObservable());
        sub.next(rta);
        sub.complete();
        return rta.success ? rta.result : [];
    }

    //
    // getUsuarios(filters: any): Observable<ServiceResponse> {
    //     return this.get(this.baseUrl + '/listar', filters);
    // }
    //
    // getEmpleados(): Observable<ServiceResponse> {
    //     return this.get('/empleado/listar');
    // }
    // //
    // saveUsuario(body: Usuario): Observable<ServiceResponse> {
    //     return this.post(this.baseUrl + '/guardar', body);
    // }
    //
    // savePassUsuario(body: Usuario): Observable<ServiceResponse> {
    //     return this.post(this.baseUrl + '/set_pass', body);
    // }
    //
    // deleteUsuario(body: Usuario): Observable<ServiceResponse> {
    //     return this.post(this.baseUrl + '/eliminar', body);
    // }


}
