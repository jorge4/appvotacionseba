import {Component, OnInit} from '@angular/core';

declare const $: any;

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const ROUTES: RouteInfo[] = [
    // {path: '/dashboard', title: 'Estadisticas', icon: 'dashboard', class: ''},
    {path: '/votar', title: 'Cargar Votos', icon: 'person', class: ''},
    {path: '/estado.punteros', title: 'Estado Punteros', icon: 'content_paste', class: ''},
    {path: '/subir.padron', title: 'Actualizar padron', icon: 'library_books', class: ''},
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
    menuItems: any[];

    constructor() {
    }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
}
