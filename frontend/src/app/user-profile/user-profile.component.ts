import {Component, OnInit, ViewChild} from '@angular/core';
import {MessageService} from 'primeng/api';
import {DatosService} from '../shared/services/datos.service';

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

    user: String;
    pass: String;
    mesa: String;
    orden: String;

    constructor(private messageService: MessageService, private datosService: DatosService) {
    }

    ngOnInit() {
    }

    evaluarKeyp(event) {
        if (event.key === 'Enter') {
            this.datosService.votar({
                user: this.user, passw: this.pass, mesa: this.mesa, orden: this.orden
            }).subscribe(r => {
                if (r.success) {
                    this.orden = '';
                }
            })
            // TODO enviar req
            // console.log('yohoo');
            // this.messageService.add({severity: 'success', summary: 'Success Message', detail: 'Order submitted'});

        }
    }

}
