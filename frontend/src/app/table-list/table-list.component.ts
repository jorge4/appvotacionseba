import {Component, OnInit} from '@angular/core';
import {TableModule} from 'primeng/table';
import {Votante} from '../shared/domain/votante';
import {MessageService} from 'primeng/api';
import {DatosService} from '../shared/services/datos.service';

@Component({
    selector: 'app-table-list',
    templateUrl: './table-list.component.html',
    styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {

    rowGroupMetadata: any;
    info_punteros: any;
    votantes_faltantes: Votante[];
    superstats: string;


    constructor(private datosService: DatosService) {
    }

    ngOnInit() {
        this.actualizarPadron();
    }

    actualizarPadron() {
        this.datosService.faltantes({}).subscribe(resp => {
            console.log(resp)
            this.votantes_faltantes = resp.result;
            this.updateRowGroupMetaData();
        });
        this.datosService.superstats({}).subscribe(resp => {
            this.superstats = resp.result;
        });
    }

    updateRowGroupMetaData() {
        this.rowGroupMetadata = {};
        if (this.votantes_faltantes) {
            for (let i = 0; i < this.votantes_faltantes.length; i++) {
                const rowData = this.votantes_faltantes[i];
                const puntero = rowData.puntero.id;
                if (i === 0) {
                    this.rowGroupMetadata[puntero] = {index: 0, size: 1};
                } else {
                    const previousRowData = this.votantes_faltantes[i - 1];
                    const punteroPrevio = previousRowData.puntero.id;
                    if (puntero === punteroPrevio) {
                        this.rowGroupMetadata[puntero].size++;
                    } else {
                        this.rowGroupMetadata[puntero] = {index: i, size: 1};
                    }
                }
            }
        }
    }


}
