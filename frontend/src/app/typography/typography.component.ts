import {Component, OnInit} from '@angular/core';
import {DatosService} from '../shared/services/datos.service';

@Component({
    selector: 'app-typography',
    templateUrl: './typography.component.html',
    styleUrls: ['./typography.component.css']
})
export class TypographyComponent implements OnInit {

    stats: string;
    user: string;
    pass: string;

    constructor(public datosService: DatosService) {
    }

    ngOnInit() {
        this.actualizar();
    }

    actualizar() {
        this.datosService.padronstats({}).subscribe(resp => {
            this.stats = resp.result;
        });
    }

    set_post_params(event: any) {
        const formData = <FormData>event.formData;

        formData.append('user', this.user);
        formData.append('passw', this.pass);
    }

    onBasicUpload(event: any) {
        this.datosService.procesarUpload(event.originalEvent);
        console.log('=> event: ', event.files);
        // if (ids.length > 0) {
        //     console.log(dato);
        //     this.datoAbiertoService.setArchivoActualizable({codigo: dato.codigo, id: ids[0]}).subscribe(rta => {
        //         if (!rta.success) {
        //             this.uploadService.cancelarSubidos({ids: ids});
        //         } else {
        //             this.getDatosActualizables();
        //         }
        //     });
        // }
    }

}
