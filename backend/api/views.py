import csv
from datetime import datetime
from io import BytesIO, TextIOWrapper

from django.db import IntegrityError, DatabaseError, transaction
# Create your views here.
from django.views.decorators.http import require_POST, require_GET

from api.models import Usuario, Votante, Voto, Puntero
from utils.BulkManager import BulkCreateManager
from utils.messaging import process_messaging_json_input, process_messaging
from utils.misc import may_fail
from utils.misc_decorators import check_for_json_args


@require_POST
@process_messaging_json_input
@check_for_json_args('user', 'passw', 'mesa', 'orden')
@may_fail(DatabaseError, "Error al guardar: Error en bd")
@may_fail(IntegrityError, "Error al guardar: Error de integridad en bd")
# @may_fail(KeyError, MsgUsuario.GUARDAR_FORMAT_ERROR)
@may_fail(Usuario.DoesNotExist, "Usuario/contraseña incorrecta")
@may_fail(Votante.DoesNotExist, "Mesa/orden incorrecto")
@transaction.atomic
def registrar_voto(request, body):
    usuario = Usuario.objects.get(username=body.user, password=body.passw)
    votante = Votante.objects.get(mesa=body.mesa, orden=body.orden)

    v = Voto.objects.filter(usuario=usuario, votante=votante).first()
    if v:
        return request.fail_with_message(
            "Voto repetido, ya cargado por {} a las {:%Y-%m-%d %H:%M}".format(v.usuario.username, v.fecha_hora))

    v = Voto(usuario=usuario, votante=votante, fecha_hora=datetime.now())
    v.save()

    return request.success_with_message("El voto se guardó con éxito")


@require_GET
@process_messaging
# @check_for_json_args('user', 'passw', 'mesa', 'orden')
@may_fail(DatabaseError, "Error al listar: Error en bd", result=[])
@may_fail(IntegrityError, "Error al listar: Error de integridad en bd", result=[])
# @may_fail(KeyError, MsgUsuario.GUARDAR_FORMAT_ERROR)
# @may_fail(Usuario.DoesNotExist, "Usuario/contraseña incorrecta")
# @may_fail(Votante.DoesNotExist, "Mesa/orden incorrecto")
def listar_votantes_faltantes(request):
    return Votante.objects.filter(voto__isnull=True, puntero__isnull=False).order_by('puntero__id', 'mesa', 'orden', 'nombre')


@require_GET
@process_messaging
# @check_for_json_args('user', 'passw', 'mesa', 'orden')
@may_fail(DatabaseError, "Error al listar: Error en bd", result=[])
def padron_stats(request):
    return '{} votantes, {} punteros, {} votos'.format(
        Votante.objects.count(),
        Puntero.objects.count(),
        Voto.objects.count()
    )


@require_GET
@process_messaging
# @check_for_json_args('user', 'passw', 'mesa', 'orden')
@may_fail(DatabaseError, "Error al listar: Error en bd", result=[])
def padron_detailed_stats(request):
    total = Votante.objects.count()
    punteros = Puntero.objects.count()
    conpuntero = Votante.objects.filter(puntero__isnull=False).count()
    votos = Voto.objects.count()
    votos_de_punteros = Votante.objects.filter(voto__isnull=False, puntero__isnull=False).count()
    votos_otros = Votante.objects.filter(voto__isnull=False, puntero__isnull=True).count()
    return '- {} votantes<br />- {} punteros<br />- {} votantes con puntero ({:3.1f}% del total)<br />' \
           '<br />- {} votos ({:3.1f}% del total)<br />- {} votos de punteros ({:3.1f}% del total)' \
           '<br />- {} votos sin puntero ({:3.1f}% del total)'.format(
        total,
        punteros,
        conpuntero,
        conpuntero / total * 100.0,
        votos,
        votos / total * 100.0,
        votos_de_punteros,
        votos_de_punteros / votos * 100.0 if votos else -1,
        votos_otros,
        votos_otros / votos * 100.0 if votos else -1,
    )


@require_POST
@process_messaging
@may_fail(Usuario.DoesNotExist, "Usuario/contraseña incorrecta")
def subir_padron(request):
    print(request.POST['user'])
    usuario = Usuario.objects.get(username=request.POST.get('user', 'dfsdf'),
                                  password=request.POST.get('passw', 'dfsdf'))
    if 'archivos' in request.FILES:
        for archivo in request.FILES.getlist('archivos'):
            print(archivo, type(archivo))
            reader = BytesIO()
            for chunk in archivo.chunks():
                reader.write(chunk)

            reader.seek(0)
            swrapper = TextIOWrapper(reader, encoding='utf-8', newline='')
            spamreader = csv.reader(swrapper)
            punteros, mesas, votantes, conpuntero = procesar_padron(spamreader)
            # for row in spamreader:
            #     print(row)
            # print(reader.tell())

            reader.close()
    # return "hola " + usuario.username
    return request.success_with_message(
        "Se cargó el padrón con exito!</br> - {} "
        "punteros<br /> - {} mesas\n - {} votantes<br> - {} votantes con puntero".format(
            punteros, mesas, votantes, conpuntero
        ))


def procesar_padron(reader):
    reader = list(reader)[1:]
    Votante.objects.all().delete()
    Voto.objects.all().delete()
    Puntero.objects.all().delete()

    punteros_nombre = set(row[7] if len(row) > 7 else '' for row in reader)
    punteros_nombre.remove('')
    punteros = {}
    for puntero in punteros_nombre:
        punteros[puntero] = Puntero.objects.create(
            nombre=puntero
        )

    current_mesa = None
    mgr = BulkCreateManager(chunk_size=2000)
    mesas = 0
    votantes = 0
    conpuntero = 0
    for i, row in enumerate(reader):
        print(row)
        if len(row) == 7:
            row = list(row) + ['']
        mesa = row[1]
        orden = row[2]
        carnet = row[3]
        nombre = row[4]
        telefono = row[5]
        ci= row[6]
        puntero=row[7]
        # _, mesa, orden, carnet, nombre, telefono, ci, puntero = row
        if mesa != '':
            current_mesa = mesa # int(mesa[1:])
            mesas += 1
            # print(current_mesa)m
        votantes += 1
        conpuntero += 1 if puntero in punteros else 0
        mgr.add(
            Votante(
                mesa=current_mesa.strip(),
                orden=int(orden),
                carnet=carnet,
                nombre=nombre,
                telefono=telefono,
                ci=ci,
                puntero=punteros.get(puntero, None)
            )
        )
    mgr.done()
    return len(punteros), mesas, votantes, conpuntero
