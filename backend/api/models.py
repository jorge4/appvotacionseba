# Create your models here.
from django.db import models

from utils.misc_decorators import add_auto_json


@add_auto_json(exclude=['password'])
class Usuario(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255, null=True)

    class Meta:
        db_table = 'usuario'


@add_auto_json
class Puntero(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=255)

    class Meta:
        db_table = 'puntero'

    def __json__(self):
        total = self.votantes.count()
        votaron = self.votantes.filter(voto__isnull=False).count()
        efectividad = float(votaron) / total * 100.0
        stats = 'Tiene {: 4d} votantes, votaron {: 4d}, faltan {: 4d}, efectividad: {: 3.1f}%'.format(
            total,
            votaron,
            total - votaron,
            efectividad,
        )
        return dict(
            stats=stats,
            total=total,
            votaron=votaron,
            faltan=total - votaron,
            efectividad=efectividad
        )


@add_auto_json
class Votante(models.Model):
    id = models.AutoField(primary_key=True)

    mesa = models.CharField(max_length=255)
    orden = models.IntegerField()

    ci = models.CharField(max_length=255)
    nombre = models.CharField(max_length=255)
    telefono = models.CharField(max_length=255)
    carnet = models.CharField(max_length=255)

    puntero = models.ForeignKey(Puntero, models.PROTECT, related_name='votantes', null=True)

    class Meta:
        db_table = 'votante'

    def __json__(self):
        return dict(puntero=self.puntero)


@add_auto_json
class Voto(models.Model):
    id = models.AutoField(primary_key=True)

    fecha_hora = models.DateTimeField()
    votante = models.OneToOneField(Votante, models.CASCADE, related_name='voto')
    usuario = models.ForeignKey(Usuario, models.PROTECT, related_name='votos')

    class Meta:
        db_table = 'voto'
