import datetime
import json

from annoying import decorators
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import QuerySet


class EntityJSONEncoder(DjangoJSONEncoder):
    """
        Clase que jsonifica entidades
    """

    def default(self, obj):
        if hasattr(obj, '__json__') and callable(getattr(obj, '__json__')):
            return obj.__json__()
        if isinstance(obj, QuerySet):
            return list(obj)
        # if isinstance(obj, Decimal):
        #    return str(obj)
        if isinstance(obj, datetime.date):
            return '{:%Y-%m-%d}'.format(obj)
        # Let the base class default method raise the TypeError
        return super(EntityJSONEncoder, self).default(obj)


decorators.FORMAT_TYPES = {
    'application/json': lambda response: json.dumps(response, cls=EntityJSONEncoder),
    'text/json': lambda response: json.dumps(response, cls=EntityJSONEncoder),
}
