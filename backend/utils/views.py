# Create your views here.
import time

from utils.messaging import process_messaging


@process_messaging
def get_ts(request):
    return int(time.time() * 1000)

