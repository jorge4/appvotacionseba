import inspect

import functools
from copy import copy
from django.db.models import Model

# from api.tablas.tabla_de_mensajes import MsgMisc

INSUFFICIENT_ARGUMENTS = "Argumentos Insuficientes"

def check_for_args(*argsu, **kwargsu):
    kwargsu['msg'] = kwargsu.get('msg', INSUFFICIENT_ARGUMENTS)
    kwargsu['result'] = kwargsu.get('result', None)

    def decorator(func):
        @functools.wraps(func)
        def wrapper(request, *args, **kwargs):
            if ('POST' in kwargsu and not (set(kwargsu['POST']) <= set(request.POST))) or \
                    ('GET' in kwargsu and not (set(kwargsu['GET']) <= set(request.GET))) or \
                    (argsu and not ((set(argsu) <= set(request.GET)) or (set(argsu) <= set(request.POST)))):
                return request.fail_with_message(kwargsu['msg'], result=kwargsu['result'])
            return func(request, *args, **kwargs)

        return wrapper

    return decorator


def check_for_json_args(*argsu, **kwargsu):
    kwargsu['msg'] = kwargsu.get('msg', INSUFFICIENT_ARGUMENTS)
    kwargsu['result'] = kwargsu.get('result', None)

    def decorator(func):
        @functools.wraps(func)
        def wrapper(request, *args, **kwargs):
            if argsu and not set(argsu) <= set(request.JSON):
                return request.fail_with_message(kwargsu['msg'], result=kwargsu['result'])
            return func(request, *args, **kwargs)

        return wrapper

    return decorator


def strip_fields(*argsu, **kwargsu):
    exclude = [] if 'exclude' not in kwargsu else kwargsu['exclude']
    if not (argsu and callable(argsu[0])) and exclude:
        raise Exception('No se puede definir include y exclude al mismo tiempo!')
    include = [] if (argsu and callable(argsu[0])) else argsu

    def decorator(func):
        @functools.wraps(func)
        def wrapper(request, *args, **kwargs):
            for field, value in request.POST.items():
                if field not in exclude and (include and field in include):
                    request.POST[field] = value.strip()
            for field, value in request.GET.items():
                if field not in exclude and (include and field in include):
                    request.GET[field] = value.strip()
            return func(request, *args, **kwargs)

        return wrapper

    return decorator(argsu[0]) if argsu and callable(argsu[0]) else decorator


def add_auto_json(*args, **kwargs):
    def decorator(cls):
        if not issubclass(cls, Model):
            raise NotImplementedError('La clase debe ser un modelo de django!')

        oldjson = getattr(cls, '__json__', None)
        exclude = [] if 'exclude' not in kwargs else kwargs['exclude']

        def newjson(self):
            try:
                a = len(newjson.fields)
            except AttributeError:
                newjson.fields = [f.name for f in cls._meta.get_fields() if
                                  f.concrete and not f.is_relation and f.name not in exclude]

            base = oldjson(self) if oldjson is not None else {}
            for field in newjson.fields:
                if field not in base:
                    base[field] = getattr(self, field, None)
                    if isinstance(base[field], Model):
                        base[field] = base[field].pk
            return base

        cls.__json__ = newjson

        return cls

    return decorator(args[0]) if args and inspect.isclass(args[0]) else decorator


def get_auto_json(model, exclude=[], base_dict={}):
    if not isinstance(model, Model):
        print('La clase debe ser un modelo de django!')
        return base_dict

    fields = [f.name for f in type(model)._meta.get_fields() if
              f.concrete and not f.is_relation and f.name not in exclude]
    base = copy(base_dict)

    for field in fields:
        if field not in base:
            base[field] = getattr(model, field, None)
            if isinstance(base[field], Model):
                base[field] = base[field].pk

    return base
