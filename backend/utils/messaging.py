import json
import logging
from json import JSONDecodeError

import functools
from annoying.decorators import ajax_request
from annoying.functions import get_config

from utils.misc import get_str_or_enum_value, objdict

logger = logging.getLogger(__name__)

"""
framework de envio de mensajes al cliente por json, estandariza el formato de retorno de datos:

```

 {

     success: true/false,

     messages: [

         {type: 'info', msg: '...'},

         ...

         {type: 'danger', msg: '...'},

     ],

     result: arbitrario

 }

```

<a href="/index.html">volver a la raiz</a>
"""


class MessagingRequest(object):
    def __init__(self, request):
        self.messaging = {'success': True, 'messages': [], 'result': None}
        self.request = request

    def __getattr__(self, attr):
        # print('getting {}', attr)
        return getattr(self.request, attr)

    def enqueue_message(self, *args, **kwargs):
        for msg in args:
            self.messaging['messages'].append({'type': 'info', 'msg': get_str_or_enum_value(msg)})
        for level, msg in kwargs.items():
            self.messaging['messages'].append({'type': level, 'msg': get_str_or_enum_value(msg)})

    def fail_with_message(self, msg, *args, result=None, **kwargs):
        self.enqueue_message(error=get_str_or_enum_value(msg).format(*args, **kwargs))
        self.messaging['success'] = False
        return result

    def success_with_message(self, msg, *args, result=None, **kwargs):
        self.enqueue_message(success=get_str_or_enum_value(msg).format(*args, **kwargs))
        self.messaging['success'] = True
        return result

    def __json__(self):
        return self.messaging


# def augment_request(request):
#     typereq = type(request)
#     if not getattr(typereq, 'ispatched', False):
#         # logger.error('Pathc!')
#         typereq.enqueue_message = enqueue_message
#         typereq.fail_with_message = fail_with_message
#         typereq.success_with_message = success_with_message
#         typereq.ispatched = True
#     request.messaging = {'success': True, 'messages': [], 'result': None}


def process_messaging(func):
    @functools.wraps(func)
    def wrapper(request, *args, **kwargs):
        # augment_request(request)
        request = MessagingRequest(request)
        try:
            request.messaging['result'] = func(request, *args, **kwargs)
        except Exception as e:
            request.messaging['result'] = None
            request.fail_with_message(repr(e))

        # if_debug_log_queries(request)
        return request

    return ajax_request(wrapper)


def process_messaging_json_input(func):
    @functools.wraps(func)
    def wrapper(request, *args, **kwargs):
        # augment_request(request)
        request = MessagingRequest(request)
        try:
            try:
                body = objdict(json.loads(request.body.decode("utf-8")))
            except (JSONDecodeError, TypeError) as exc:
                body = {}
                if get_config('DEBUG', False):
                    request.enqueue_message(info='DEBUG=true, Excepcion: {}'.format(repr(exc)))
            request.JSON = body
            request.messaging['result'] = func(request, body, *args, **kwargs)
        except Exception as e:
            request.messaging['result'] = None
            request.fail_with_message(repr(e))

        # if_debug_log_queries(request)
        return request

    return ajax_request(wrapper)
